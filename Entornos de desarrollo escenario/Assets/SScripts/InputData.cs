﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public struct InputData
{
	//mov
	public float hMovement;
	public float vMovement;
	//Mouse rot
	public float verticalMouse;
	public float horizontalMouse;
	//extra
	public bool dash;
	public bool jump;

	public void getInput()
	{
		hMovement = Input.GetAxis("Horizontal");
		vMovement = Input.GetAxis("Vertical");

		verticalMouse = Input.GetAxis("Mouse Y");
		horizontalMouse = Input.GetAxis("Mouse X");

		dash = Input.GetButton("Dash");
		jump = Input.GetButton("Jump");
	}

}
