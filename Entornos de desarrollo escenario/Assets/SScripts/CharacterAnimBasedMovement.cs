﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]
public class CharacterAnimBasedMovement : MonoBehaviour
{
    public float rotationSpeed = 4f;
    public float rotationThreshold = 0.3f;
    [Range(0, 180f)]
    public float degreesToTurn = 160f;
    public float idleTimer = 0f;
    public float timer = 0f;
    public float timeSelection = 0f;

    [Header("Animator Parameters")]
    public string motionParam = "motion";
    public string mirrorIdleParam = "mirrorIdle";
    public string turn180Param = "turn180";
    public string idle_typeParam = "idle_type";
    public string jumpAnimParam = "jumpAnim";

    [Header("Animation Smoothing")]
    [Range(0, 1f)]
    public float StartAnimTime = 0.3f;
    [Range(0, 1f)]
    public float StopAnimTime = 0.15f;

    //private Ray wallRay = new Ray();
    private float Speed;
    private Vector3 desiredMoveDirection;
    private CharacterController characterController;
    private Animator animator;
    private bool mirrorIdle;
    private bool turn180;
    private bool jumpAnim;


	void Start()
	{
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
	}

    public void moveCharacter(float hInput, float vInput, Camera cam, bool jump, bool dash)
	{
        //Calc Input
        Speed = new Vector2(hInput, vInput).normalized.sqrMagnitude;

        if (Input.GetButtonDown("Jump") && this.animator.GetCurrentAnimatorStateInfo(0).IsName("Jump") == false)
		{
            animator.SetBool("jumpAnim", true);
		}
        else if (this.animator.GetCurrentAnimatorStateInfo(0).IsName("Jump") == true)
		{
            animator.SetBool("jumpAnim", false);
        }
        //dash only if max speed
        if (Speed >= Speed - rotationThreshold && dash)
		{
            Speed = 1.5f;
		}

        //phys move player
        if (Speed > rotationThreshold)
		{
            animator.SetFloat(motionParam, Speed, StartAnimTime, Time.deltaTime);
            Vector3 forward = cam.transform.forward;
            Vector3 right = cam.transform.right;

            forward.y = 0f;
            right.y = 0f;

            forward.Normalize();
            right.Normalize();

            //rotate

            desiredMoveDirection = forward * vInput + right * hInput;

            if (Vector3.Angle(transform.forward, desiredMoveDirection) >= degreesToTurn)
            {
                turn180 = true;
            }
            else
            {

                turn180 = false;
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), rotationSpeed * Time.deltaTime);
            }
            //180turning
            animator.SetBool(turn180Param, turn180);
            //movechar
            animator.SetFloat(motionParam, Speed, StartAnimTime, Time.deltaTime);
            

		}
        else if (Speed < rotationThreshold)
		{
            animator.SetBool(mirrorIdleParam, mirrorIdle);
            animator.SetFloat(motionParam, Speed, StopAnimTime, Time.deltaTime);
            //idle selection

            timeSelection = Random.Range(15f, 45f);
           if (timer >= timeSelection)

			{
                Invoke("ChangeIdle", timeSelection);
                timer = 0f;
               
            }
			else
			{
                
                timer += Time.deltaTime;
			}
           // animator.SetFloat(idle_typeParam, idleTimer);
		}
	}
	private void OnAnimatorIK(int layerIndex)
	{
        if (Speed < rotationThreshold) return;
        float distanceToLeftFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.LeftFoot));
        float distanceToRightFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.RightFoot));

        if(distanceToRightFoot > distanceToLeftFoot)
		{
            mirrorIdle = true;
		}
        else
		{
            mirrorIdle = false;
		}
    }
    public void ChangeIdle()
	{
        idleTimer = Random.Range(0f, 2f);
        animator.SetFloat(idle_typeParam, idleTimer);
    }
    
	

}
